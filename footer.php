	    <div class="row"><!-- Rangée pour le menu pied de page --><!-- Row for footer menu -->
	        <footer class="container navbar navbar-inverse"><!-- Début de la barre de navigation Bootstrap --><!-- Start of Bootstrap navbar -->
	          <div id="footermenu">
	            <?php
	            wp_nav_menu( array(
	              'menu' => 'Menu du bas',
	              'depth' => 2,
	              'container' => true,
	              'menu_class' => 'nav navbar-nav dropup',
	              'walker' => new wp_bootstrap_navwalker())
	            );
	            ?><!-- wp_bootstrap_navwalker pour la génération du pied de page --><!-- wp_bootstrap_navwalker for footer generation -->
	          </div>
	        </footer><!-- fin de la rangée du menu --><!-- end of the menu row -->
	    </div><!-- Fin de la rangée pour le menu pied de page --><!-- End of row for footer menu -->
	    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
	</div><!-- Fin du onteneur de la page --><!-- End of page wrapper -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <?php wp_footer(); ?>
  </body>
</html>
