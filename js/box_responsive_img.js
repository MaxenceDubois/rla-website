$(".test").each(function(){
    // Uncomment the following if you need to make this dynamic
    var refH = $(this).height();
    var refW = $(this).width();
    var refRatio = refW/refH;


    var imgH = $(".bordel").height();
    var imgW = $(".bordel").width();

    if ( (imgW/imgH) < refRatio ) {
        $(this).addClass("portrait");
    } else {
        $(this).addClass("landscape");
    }
})
