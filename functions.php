<?php
require_once('inc/wp_bootstrap_navwalker.php');
require_once('inc/breadcrumb.php');

// Créer les paramètres pour le bandeau thématique
// Create settings for the thematic band
function band_theme_customizer( $wp_customize ) {
	$wp_customize->add_section( 'band' , array(
		'title'    => 'Bandeau'
	) );

	$wp_customize->add_setting( 'boxes');

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'boxes', array(
		'label'    => __( 'Nombre de cases', 'mytheme' ),
		'section'  => 'band',
		'settings' => 'boxes',
		'type'		 => 'number',
		'input_attrs' => array(
	      'min'   => 1,
	      'max'   => 4,
	) ) ) );

	$i=1;
	$y =  get_theme_mod('boxes');

	while ($i <= $y) {

		$wp_customize->add_setting( 'image' . $i);

		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image' . $i, array(
				'label'    => __( 'Modifier image ' . $i, 'mytheme' ),
				'section'  => 'band',
				'settings' => 'image' . $i,
		) ) );

		$wp_customize->add_setting( 'label' . $i);

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'label' . $i, array(
			'label'    => __( 'Libellé de la case ' . $i, 'mytheme' ),
			'section'  => 'band',
			'settings' => 'label' . $i,
			'type'		 => 'text',
			) ) );

			$i++;

	}

}

add_action( 'customize_register', 'band_theme_customizer' );

// Créer les paramètres pour la page d'accueil
// Create settings for home page
function home_theme_customizer( $wp_customize ) {
	$wp_customize->add_section( 'home' , array(
		'title'    => 'Accueil'
	) );

	$wp_customize->add_setting( 'extract_word');

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'extract_word', array(
			'label'    => __( 'Nombre de mots extrait d\'un article sur la page d\'accueil :', 'mytheme' ),
			'section'  => 'home',
			'settings' => 'extract_word',
			'type'		 => 'number',
	) ) );

}

add_action( 'customize_register', 'home_theme_customizer' );

add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

// Changer le nombre de mots affichés dans l'extrait sur l'accueil
// Change nomber of word chown in the excerpt on home page
function wpdocs_custom_excerpt_length( $length ) {
  return get_theme_mod('extract_word');
}

add_theme_support( 'post-thumbnails' );

// Créer les paramètres pour l'en-tête du site
// Create settings for website's header
function mytheme_setup() {

  add_theme_support( 'custom-logo', array(
  	'height'      => 100,
  	'flex-width'  => true,
  	'header-text' => array( 'site-title', 'site-description' ),
  ) );

  register_nav_menu( 'primary', __( 'Primary navigation', 'main' ) );

	register_nav_menu( 'secondary', __( 'Menu case 1') );
	register_nav_menu( 'secondary', __( 'Menu case 2') );
	register_nav_menu( 'secondary', __( 'Menu case 3') );
  register_nav_menu( 'secondary', __( 'Menu case 4') );

	register_nav_menu( 'secondary', __( 'Secondary navigation', 'sub' ) );

}

add_action( 'after_setup_theme', 'mytheme_setup' );

// Charger les scripts et les feuilles de style
// Load scripts and stylesheets
function startwordpress_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6' );
	wp_enqueue_style( 'blog', get_template_directory_uri() . '/style.css' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );
	wp_enqueue_script( 'back_to_top', get_template_directory_uri() . '/js/back_to_top.js', array( 'jquery' ), '3.3.6', true );
	wp_enqueue_script( 'box_number_band', get_template_directory_uri() . '/js/box_overlay.js', array( 'jquery' ), '3.3.6', true );
	wp_enqueue_script( 'box_responsive_img', get_template_directory_uri() . '/js/box_responsive_img.js', array( 'jquery' ), '3.3.6', true );
}

add_action( 'wp_enqueue_scripts', 'startwordpress_scripts' );
?>
