<!DOCTYPE html>
<html>
  <head>
		<html lang="fr">
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('&raquo;','true','right'); ?><?php bloginfo('name'); ?></title>
  	<meta name="description" content="<?php echo get_option('fullby_description'); ?>">
  	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    <?php wp_head();?>
  </head>
  <body>
    <div class="container page"><!-- Conteneur de la page --><!-- Page wrapper -->
      <div class="header">
        <div class="row logo"><!-- Rangée d'en-tête pour le logo --><!-- Header row for the logo -->
          <div class="logo-rla col-sm-3">
            <?php the_custom_logo(); ?><!-- Fonction custom-logo de Wordpress --><!-- custom-logo function of Wordpress -->
          </div>
          <div class="contact-header hidden-xs hidden-sm col-sm-offset-6 col-sm-3"><!-- Block des contacts --><!-- Contacts' block -->
            <p>20 rue de la Monnaie <br> 59 000 Lille</p>
            <p>03.20.51.43.57</p>
	    			<p>lille.ancien@orange.fr</p>
          </div><!-- Fin du block des contacts --><!-- end of contacts' block -->
        </div>
        <div class="row site-description"><!-- Description du site --><!-- Website description -->
          <p>Association Loi 1901 fondée en 1964 – Lille métropole et région <br>
	  			Connaissance et Protection du Patrimoine culturel et naturel, matériel et immatériel.</p>
        </div><!-- Fin de la description --><!-- End of description -->
      </div>

      <div class="row"><!-- Rangée pour le mnu principal --><!-- Row for main menu -->
	      <nav class="navbar navbar-default"><!-- Début de la barre de navigation Bootstrap --><!-- Start of Bootstrap navbar -->
		  <div class="navbar-header">
		    <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target=".navbar-collapse">
		      <div class="button-bars">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		      </div>
		      <span class="button-label">Dérouler les onglets</span>
		    </button>
		  </div>
		  <div id="mainmenu" class="collapse navbar-collapse">
		    <?php
		  		wp_nav_menu( array(
		  		  'menu' => 'Menu du haut',
		  		  'depth' => 2,
		  		  'container' => true,
		  		  'menu_class' => 'nav navbar-nav',
		  		  'walker' => new wp_bootstrap_navwalker())
		  		);
				?><!-- wp_bootstrap_navwalker pour la génération du menu --><!-- wp_bootstrap_navwalker for menu generation -->

							<!-- Formulaire pour le champ de recherche Wordpress -->
							<!-- Form for Wordpress search field -->
		    <form class="navbar-form navbar-right" role="search" method="get" action="<?php echo home_url() ; ?>">
		      <div class="form-group">
			<input type="text" class="form-control" placeholder="Search" name="s" id="srch-term">
		      </div>
		      <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		    </form>
		  </div>
	      </nav>
    	</div><!-- fin de la rangée du menu --><!-- end of the menu row -->
