			<?php

			if ( has_post_thumbnail() ) {
			  the_post_thumbnail();
			}

			breadcrumb(); // Afficher le fil d'arianne avec Bootstrap. Show breadcrumn with Bootstrap

			?>

			<div class="blog-post col-xs-offset-1 col-xs-10"><!-- Bloc pour afficher l'article --><!-- Block for article -->
				<h1 class="blog-post-title"><?php the_title(); ?></h1>
				<p class="blog-post-meta"><?php the_date(); ?> by <a href="#"><?php the_author(); ?></a></p>

				<?php the_content(); ?>

			</div>
