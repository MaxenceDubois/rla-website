<?php get_header(); ?>

			<div class="row"><!-- Rangée pour le bandeau thématique --><!-- Row for thematic band -->
			  <div id="effect-1" class="effects">
				  <p class="worktitle">Nos axes de travail</p>
			  <?php

			  $boxes_num = get_theme_mod( 'boxes'); // Nombre de case souhaitée. Number of box wanted.
			  $i=1;

			  while ($i <= $boxes_num) { // Boucle pour créer les différents bloc de chaque case. Loop to create blocs for each box.
			  ?>
			    <div class="band hidden-xs hidden-sm col-md-<?php echo 12 / $boxes_num; ?>"><!-- Le bandeau est caché sur mobile --><!-- band is hidden on mobile -->
			      <div class="img test band-height">
			        <img class="bordel" src="<?php echo get_theme_mod( 'image' . $i );?>">
			        <div class="overlay"><!-- Bloc du panneau couvrant --><!-- Overlay's block -->
			          <div class="container overlay-links">
			            <?php
			            wp_nav_menu( array(
			              'menu' => 'Menu case ' . $i,
			            ) );
			            ?><!-- Utilisation de la fonction de menu de Wordpress --><!-- Use of Wordpress' menu fonction -->
			          </div>
			        </div><!-- Fin du bloc du panneau couvrant --><!-- End of overlay's block -->
			      </div>
			      <div><!-- Bloc pour le titre de la thematique --><!-- Bloc for themtic label -->
			        <p class="theme-label"><?php echo get_theme_mod( 'label' . $i );?></p>
			      </div>
			    </div>
			    <?php $i++;   } ?>
			  </div>
			</div><!-- Fin de la rangée pour le bandeau thématique --><!-- End of row for thematic band -->
			<div class="row">
			  <div class="news"><!-- Bloc pour les actualités --><!-- Bloc for news -->
				  <p class="worktitle">Nos dernières actualités</p>
			    <?php

			    $columns_num = 3; // Nombre de colonnes souhaité pour les posts. The number of columns wanted to display posts.
			    $i = 0; //Counter for .row divs

			    echo '<div class="row articles">';

			        /* Start the Loop */
			        while ( have_posts() ) : the_post();
			    ?>
			    <div class="single-product-archive col-md-<?php echo 12 / $columns_num; ?>"><!-- Taille des colonnes en fonction de leur nombre --><!-- Size of columns depending their number. -->
			    	<?php get_template_part('excerpt', get_post_format() ); ?>
			    </div>
			    <?php
			            if($i % $columns_num == $columns_num - 1 ) {
			                echo '</div><div class="row articles">'; // Créer une nouvelle rangée toutes les trois actualités. Create a row each 3 news.
			            }

			            $i++;

			        endwhile;

			    ?>
			    <nav>
			    	<ul class="pager col-xs-12">
			    		<li><?php next_posts_link( 'Articles plus anciens' ); ?></li>
			    		<li><?php previous_posts_link( 'Articles plus récents' ); ?></li>
			    	</ul>
			    </nav>
			    </div>
			  </div><!-- Fin du bloc pour les actualités --><!-- End of bloc for news -->
			</div>

<?php get_footer(); ?>
