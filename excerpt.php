			<!-- Afficher l'extrait de l'article --><!-- Show article excerpt -->
			<p class="category"> <?php the_category( '&gt; ' ); ?> </p>
			<?php if ( has_post_thumbnail() ) {the_post_thumbnail();} ?>
			<h2 class="excerpt-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<p class="excerpt-meta"><?php the_date(); ?> by <a href="#"><?php the_author(); ?></a></p>

			<?php the_excerpt(); ?>

			<p><a href="<?php the_permalink(); ?>">Lire la suite...</a></p>
